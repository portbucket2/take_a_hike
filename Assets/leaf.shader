Shader "Custom/Leaf" {
   Properties 
   {
      _BumpMap ("Normal Map", 2D) = "bump" {}
      _DiffuseMap("Diffuse Map", 2D) = "white"{}
      _Color ("Diffuse Material Color", Color) = (1,1,1,1)
   }

      CGINCLUDE // common code for all passes of all subshaders
      //#pragma target 3.0
      #pragma multi_compile_fog
      #include "UnityCG.cginc"
      
      uniform float4 _LightColor0; 
      // color of light source (from "Lighting.cginc")

      // User-specified properties
      uniform sampler2D _BumpMap;   
      uniform float4 _BumpMap_ST;
      uniform sampler2D _DiffuseMap;   
      uniform float4 _DiffuseMap_ST;
      uniform float4 _Color;

      struct vertexInput 
      {
         float4 vertex : POSITION;
         float4 texcoord : TEXCOORD0;
         float3 normal : NORMAL;
         float4 tangent : TANGENT;
      };


      struct vertexOutput {
         float4 pos : SV_POSITION;
         float4 posWorld : TEXCOORD0;
         float4 tex : TEXCOORD1;
         float3 tangentWorld : TEXCOORD2;  
         float3 normalWorld : TEXCOORD3;
         float3 binormalWorld : TEXCOORD4;
         UNITY_FOG_COORDS(5)
      };

      vertexOutput VertFunc(vertexInput input) 
      {
         vertexOutput output;

         float4x4 modelMatrix = unity_ObjectToWorld;
         float4x4 modelMatrixInverse = unity_WorldToObject;

         output.tangentWorld = normalize(mul(modelMatrix, float4(input.tangent.xyz, 0.0)).xyz);
         output.normalWorld = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
         output.binormalWorld = normalize(cross(output.normalWorld, output.tangentWorld) * input.tangent.w); // tangent.w is specific to Unity
         output.tex = input.texcoord;
         output.pos = UnityObjectToClipPos(input.vertex);
         output.posWorld = mul(modelMatrix, input.vertex);
         UNITY_TRANSFER_FOG(output,output.pos);
         return output;
      }

      // fragment shader with ambient lighting
      float4 FragFunc(vertexOutput input) : COLOR
      {
         // in principle we have to normalize tangentWorld,
         // binormalWorld, and normalWorld again; however, the 
         // potential problems are small since we use this 
         // matrix only to compute "normalDirection", 
         // which we normalize anyways
         float4 baseCol = tex2D(_DiffuseMap, _DiffuseMap_ST.xy * input.tex.xy + _DiffuseMap_ST.zw);
         float4 encodedNormal = tex2D(_BumpMap, _BumpMap_ST.xy * input.tex.xy + _BumpMap_ST.zw);
         float3 localCoords = float3(2.0 * encodedNormal.a - 1.0, 2.0 * encodedNormal.g - 1.0, 0.0);
         //localCoords.z = sqrt(1.0 - dot(localCoords, localCoords));
         localCoords.z = 1.0 - 0.5 * dot(localCoords, localCoords);

         float3x3 local2WorldTranspose = float3x3(input.tangentWorld, input.binormalWorld, input.normalWorld);
         float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));
         float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
         float3 diffuseReflection = _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));
         float4 finalColor = float4(1,1,1,0);
         finalColor.rgb = diffuseReflection;
         finalColor.rgb += normalize(_Color.rgb * UNITY_LIGHTMODEL_AMBIENT.rgb);
         finalColor.rgb *= baseCol.rgb;
         finalColor.rgb = (finalColor.rgb);
         finalColor.a = baseCol.a;
         
         float viewDistance = length(_WorldSpaceCameraPos - input.posWorld);
         UNITY_CALC_FOG_FACTOR_RAW(viewDistance);
         return lerp(unity_FogColor, finalColor, unityFogFactor);
         
      }
      
   ENDCG

   SubShader {
        
      Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
      LOD 300
      ZWrite Off ColorMask RGB
      Cull Off
      Pass {      
         Name "FORWARD"
         Tags { "LightMode" = "ForwardBase" }
         Blend SrcAlpha OneMinusSrcAlpha
         CGPROGRAM
         #pragma vertex VertFunc  
         #pragma fragment FragFunc
         ENDCG
      }
   }
}