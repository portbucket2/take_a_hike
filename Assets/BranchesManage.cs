﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchesManage : MonoBehaviour
{
    public List<GameObject> BranchesOfTree;
    public static BranchesManage instance;

    public GameObject BranchPrefab;
    //public GameObject BranchPrefabSubBranchable;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddMeToBranchesList(GameObject branch)
    {
        BranchesOfTree.Add(branch);
    }

    public void BranchInstantiatingAtDirection(int side , float Angle , Transform thisRoot,float ParentScale, float Scale, int subBranchesIndex)
    {
        GameObject go = Instantiate(BranchPrefab, thisRoot.position, thisRoot.rotation);
        go.transform.SetParent(thisRoot.transform);
        go.GetComponent<BranchCreationSystem>().subBranchIndex = subBranchesIndex - 1;
        if(go.GetComponent<BranchCreationSystem>().subBranchIndex > 0)
        {
            go.GetComponent<BranchCreationSystem>().subBranching = true;
        }
        else
        {
            go.GetComponent<BranchCreationSystem>().subBranching = false;
        }
        go.GetComponent<BranchCreationSystem>().currentHexScale= ParentScale *Scale;

        
        go.transform.localScale = Vector3.one * Scale;
        go.transform.localRotation =Quaternion.Euler( new Vector3(0, 0, Angle));

        if (!go.GetComponent<BranchCreationSystem>().subBranching)
        {
            BranchesManage.instance.AddMeToBranchesList(go.gameObject);
        }
        
    }
}
