﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    [SerializeField] float walkSpeed;
    float walkSpeedApp;
    public float maxRotSpeed;
    float rotSpeedApp;
    public Vector2 trackPos;
    public GameObject camHolder;

    float camRotApp;
    //public float maxcamRot;
    public float clickInY;
    public float camRotProgress;
    public float camRotOld;
    public float camRotNew;
    public float snapSpeed;

    public bool GroundHit;
    float YPos;
    public float height;
    // Start is called before the first frame update
    void Start()
    {
        ResetCameraTransform();
    }

    // Update is called once per frame
    void Update()
    {
        Raycastingg();
        Debug.Log("on update method.");
        ManageScreenTapPoint();
        ManageCamHolderRot();
        
        ManageWalk();

        





    }

    void ManageScreenTapPoint()
    {
        trackPos.x = (Input.mousePosition.x / Screen.width) - 0.5f;
        trackPos.y = (Input.mousePosition.y / Screen.height);
        Debug.Log("on manage tap point-"+Input.mousePosition);
    }

    void ManageWalk()
    {
        if (Input.GetMouseButton(0))
        {
            Debug.Log("mouse button down");
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, walkSpeed, Time.deltaTime * snapSpeed);
            if(Mathf.Abs(trackPos.x) > 0.2f)
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, trackPos.x * maxRotSpeed, Time.deltaTime * snapSpeed);
            }
            else
            {
                rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            }

            camRotProgress = clickInY - trackPos.y;
            camRotNew = camRotOld + camRotProgress * 100f;
            camRotNew = Mathf.Clamp(camRotNew, -90f, 90f);
            //camRotApp = camRotNew;


            camRotApp = Mathf.Lerp(camRotApp, camRotNew, Time.deltaTime *snapSpeed);
        }
        else
        {
            walkSpeedApp = Mathf.Lerp(walkSpeedApp, 0, Time.deltaTime * snapSpeed);
            rotSpeedApp = Mathf.Lerp(rotSpeedApp, 0, Time.deltaTime * snapSpeed);
            camRotProgress = 0;
            camRotApp = Mathf.Lerp(camRotApp, camRotNew, Time.deltaTime * snapSpeed);
            camRotNew = Mathf.Clamp(camRotNew, -90f, 90f);

        }

        transform.Translate(0, 0, walkSpeedApp*(1 - Mathf.Abs(trackPos.x * 2)) * Time.deltaTime * 50f);
        transform.Rotate(0, rotSpeedApp * Time.deltaTime * 50f, 0);

        transform.position = new Vector3(transform.position.x, YPos, transform.position.z);
    }

    void ManageCamHolderRot()
    {
        camHolder.transform.localRotation = Quaternion.Euler(camRotApp, 0 ,0);
        if (Input.GetMouseButtonDown(0))
        {
            camRotOld = camRotNew;
            clickInY = trackPos.y;
        }
        if (Input.GetMouseButtonUp(0))
        {
            
        }
    }

    void ResetCameraTransform()
    {
        camHolder.transform.localPosition = (new Vector3(0,0,0));
        camHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }


    void Raycastingg()
    {   
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Did Hit");
            GroundHit = true;
            YPos = hit.point.y + height;
        }
        else
        {
            GroundHit = false;
        }
    }
}
