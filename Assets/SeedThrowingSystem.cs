﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedThrowingSystem : MonoBehaviour
{
    public GameObject TreeObj;
    public GameObject BaseBush;
    public GameObject seedObj;
    public ParticleSystem soilSplashParticle;
    public ParticleSystem seedBackParticle;

    public GameObject TapToThrowText;

    public bool RaiseTree;
    public bool SeedThrown;
    public float Scale;
    // Start is called before the first frame update
    void Start()
    {
        BaseBush.transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        RaiseTheBush();

        if (Input.GetMouseButtonDown(0) && !SeedThrown) 
        {
            ThrowSeed();
            //EnableTree();
        }
        
    }

    void ThrowSeed()
    {
        seedObj.AddComponent<Rigidbody>();

        seedObj.GetComponent<Rigidbody>().AddForce(650 * seedObj.transform.forward + 400 * seedObj.transform.up);
        seedObj.GetComponent<Rigidbody>().AddTorque(950 * seedObj.transform.right + 400 * seedObj.transform.up);

        Invoke("EnableTree", 1.1f);
        SeedThrown = true;
        TapToThrowText.SetActive(false);
        seedBackParticle.gameObject.SetActive(false);
    }


    void EnableTree()
    {
        TreeObj.SetActive(true);
        soilSplashParticle.Play();
        RaiseTree = true;

    }

    void RaiseTheBush()
    {

        if (RaiseTree)
        {
            if (Scale < 1)
            {
                Scale += Time.deltaTime * 1f;
            }
            else
            {
                Scale = 1;
            }
            seedObj.transform.localScale = Vector3.Lerp(seedObj.transform.localScale, Vector3.zero * Scale, Time.deltaTime * 10);
        }


        BaseBush.transform.localScale = Vector3.Lerp(BaseBush.transform.localScale, Vector3.one * Scale, Time.deltaTime * 5);
        
    }
    
}
