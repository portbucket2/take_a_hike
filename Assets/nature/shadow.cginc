#ifndef PORTBLISS_SHADOW_INCLUDED
#define PORTBLISS_SHADOW_INCLUDED

	float4 UnityClipSpaceShadowCasterPos(float4 vertex, float3 normal)
	{
		float4 wPos = mul(unity_ObjectToWorld, vertex);
		if (unity_LightShadowBias.z != 0.0)
		{
			float3 wNormal = UnityObjectToWorldNormal(normal);
			float3 wLight = normalize(UnityWorldSpaceLightDir(wPos.xyz));

			// apply normal offset bias (inset position along the normal)
			// bias needs to be scaled by sine between normal and light direction
			// (http://the-witness.net/news/2013/09/shadow-mapping-summary-part-1/)
			//
			// unity_LightShadowBias.z contains user-specified normal offset amount
			// scaled by world space texel size.

			float shadowCos = dot(wNormal, wLight);
			float shadowSine = sqrt(1 - shadowCos * shadowCos);
			float normalBias = unity_LightShadowBias.z * shadowSine;
			wPos.xyz -= wNormal * normalBias;
		}
		return mul(UNITY_MATRIX_VP, wPos);
	}																									

	float4 UnityApplyLinearShadowBias(float4 clipPos)
	{
		// For point lights that support depth cube map, the bias is applied in the fragment shader sampling the shadow map.
		// This is because the legacy behaviour for point light shadow map cannot be implemented by offseting the vertex position
		// in the vertex shader generating the shadow map.
		#if !(defined(SHADOWS_CUBE) && defined(SHADOWS_CUBE_IN_DEPTH_TEX))
			#if defined(UNITY_REVERSED_Z)
				// We use max/min instead of clamp to ensure proper handling of the rare case
				// where both numerator and denominator are zero and the fraction becomes NaN.
				clipPos.z += max(-1, min(unity_LightShadowBias.x / clipPos.w, 0));
			#else
				clipPos.z += saturate(unity_LightShadowBias.x / clipPos.w);
			#endif
		#endif

		#if defined(UNITY_REVERSED_Z)
			float clamped = min(clipPos.z, clipPos.w*UNITY_NEAR_CLIP_VALUE);
		#else
			float clamped = max(clipPos.z, clipPos.w*UNITY_NEAR_CLIP_VALUE);
		#endif
			clipPos.z = lerp(clipPos.z, clamped, unity_LightShadowBias.y);
			return clipPos;
	}
#endif