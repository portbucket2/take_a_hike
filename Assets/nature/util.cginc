#ifndef PORTBLISS_SHADER_UTILITY_INCLUDED
#define PORTBLISS_SHADER_UTILITY_INCLUDED

	inline fixed3 UnpackNormalDXT5nm(fixed4 packednormal)
	{
		fixed3 normal;
		normal.xy = packednormal.wy * 2 - 1;
		normal.z = packednormal.x;// sqrt(1 - saturate(dot(normal.xy, normal.xy)));
		return normal;
	}

	// Transforms direction from object to world space
	inline fixed3 UnityObjectToWorldDir(in fixed3 dir)
	{
		return normalize(mul((fixed3x3)unity_ObjectToWorld, dir));
	}

	// Transforms normal from object to world space
	inline fixed3 UnityObjectToWorldNormal(in fixed3 norm)
	{
		#ifdef UNITY_ASSUME_UNIFORM_SCALING
			return UnityObjectToWorldDir(norm);
		#else
			// mul(IT_M, norm) => mul(norm, I_M) => {dot(norm, I_M.col0), dot(norm, I_M.col1), dot(norm, I_M.col2)}
			return normalize(mul(norm, (fixed3x3)unity_WorldToObject));
		#endif
	}

	
	// Computes world space view direction, from object space position
	inline fixed3 UnityWorldSpaceViewDir(in fixed3 worldPos)
	{
		return _WorldSpaceCameraPos.xyz - worldPos;
	}

	// Computes world space light direction, from world space position
	inline float3 UnityWorldSpaceLightDir(in float3 worldPos)
	{
		#ifndef USING_LIGHT_MULTI_COMPILE
				return _WorldSpaceLightPos0.xyz - worldPos * _WorldSpaceLightPos0.w;
		#else
			#ifndef USING_DIRECTIONAL_LIGHT
					return _WorldSpaceLightPos0.xyz - worldPos;
			#else
					return _WorldSpaceLightPos0.xyz;
			#endif
		#endif
	}
#endif