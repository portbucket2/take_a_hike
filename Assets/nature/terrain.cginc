#ifndef PORTBLISS_TERRAIN_ENGINE_INCLUDED
#define PORTBLISS_TERRAIN_ENGINE_INCLUDED

	struct SurfaceOutput 
	{
		fixed3 Albedo;
		fixed3 Normal;
		fixed Alpha;
	};

	inline fixed4 LightingBush(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed4 lightCol)
	{
		half nl = dot(s.Normal, lightDir);
		nl = max(0, nl * 0.6 + 0.4);
		fixed4 c;
		c.rgb = s.Albedo *(nl);
		c.rgb = c.rgb * lightCol.rgb;
		return c;
	}

	inline fixed4 LightingTree(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed4 lightCol)
	{
		half nl = dot(s.Normal, lightDir);
		nl = max(0, nl * 0.6 + 0.4);
		fixed4 c;
		c.rgb = s.Albedo * nl * lightCol.rgb;
		c.a = 1;
		return c;
	}

	inline fixed4 LightingStone(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed4 lightCol)
	{
		half nl = dot(s.Normal, lightDir);
		nl = max(0, nl * 0.6 + 0.4);
		fixed4 c;
		c.rgb = s.Albedo * nl * lightCol.rgb;
		c.a = 1;
		return c;
	}

	inline fixed4 LightingGrass(SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed4 lightCol)
	{
		half nl = dot(s.Normal, lightDir);
		nl = max(0, nl * 0.6 + 0.4);
		fixed4 c;
		c.rgb = s.Albedo * nl * lightCol.rgb;
		c.a = 1;
		return c;
	}
#endif