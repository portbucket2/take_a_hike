﻿// Upgrade NOTE: replaced 'defined FOG_COMBINED_WITH_WORLD_POS' with 'defined (FOG_COMBINED_WITH_WORLD_POS)'
// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Portbliss/Stone" 
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_Additive("Additive vertex pass", Range(0,5)) = 0.2

		// These are here only to provide default values
		[HideInInspector] _TreeInstanceScale("no idea why it has to be here now", Vector) = (1,1,1,1)
	}

		SubShader
		{
			Tags
			{
				"IgnoreProjector" = "True"
			}
			LOD 200
			Pass
			{
				Name "FORWARD"
				Tags { "LightMode" = "ForwardBase" }
				ColorMask RGB

				CGPROGRAM
				// compile directives
				#pragma vertex vert_surf
				#pragma fragment frag_surf
				#pragma multi_compile_fog
				#pragma multi_compile_fwdbase
				#include "HLSLSupport.cginc"
				#include "fog.cginc"
				#include "util.cginc"
				#include "sh.cginc"
				#include "terrain.cginc"
				
				struct appdata_stone
				{
					float4 vertex : POSITION;
					fixed3 normal : NORMAL;
					fixed4 texcoord : TEXCOORD0;
				};

				fixed _Additive;
				fixed _Cutoff;
				fixed4 _LightColor0;//updated by unity
				fixed4 _Color;

				// vertex-to-fragment interpolation data
				struct v2f
				{
					float4 pos : SV_POSITION;
					fixed2 uv : TEXCOORD0;
					fixed3 vlight : TEXCOORD1;
					fixed fogCoord : TEXCOORD2;
				};

				// vertex shader
				v2f vert_surf(appdata_stone v)
				{
					v2f o;
					o = (v2f)0;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.texcoord;
					fixed3 normalWorld = UnityObjectToWorldNormal(v.normal);
					half nl = dot(normalWorld, _WorldSpaceLightPos0.xyz);
					nl = max(0, nl * 0.6 + 0.4);

					fixed3 shlight = ShadeSH9(fixed4(normalWorld, 1.0));
					o.vlight = shlight * _Color * nl * _LightColor0.rgb;
					o.vlight += fixed3(_Additive, _Additive, _Additive);
					float z = o.pos.z;
					o.fogCoord.x = FOG_FACTOR(GET_PROPER_Z_DIST(z));
					return o;
				}

				// fragment shader
				fixed4 frag_surf(v2f IN) : SV_Target
				{
					fixed4 col = fixed4(IN.vlight.rgb, 1);
					col.rgb = lerp(unity_FogColor.rgb, col.rgb, saturate(IN.fogCoord.x));
					return col;
				}
			ENDCG
		}

			// Pass to render object as a shadow caster
			Pass
			{
				Name "ShadowCaster"
				Tags { "LightMode" = "ShadowCaster" }

				CGPROGRAM
				#pragma vertex vert_surf
				#pragma fragment frag_surf
				#pragma multi_compile_shadowcaster
				#include "HLSLSupport.cginc"
				#include "util.cginc"
				#include "shadow.cginc"

				sampler2D _MainTex;
				fixed _Cutoff;

				struct appdata_stone
				{
					float4 vertex : POSITION;
					fixed3 normal : NORMAL;
					fixed2 texcoord : TEXCOORD0;
				};

				struct v2f_surf
				{
					float4 pos : SV_POSITION;
					fixed2 hip_pack0 : TEXCOORD1;
				};

				v2f_surf vert_surf(appdata_stone v)
				{
					v2f_surf o;
					o.hip_pack0.xy = v.texcoord;
					o.pos = UnityClipSpaceShadowCasterPos(v.vertex, v.normal);
					o.pos = UnityApplyLinearShadowBias(o.pos);
					return o;
				}

				fixed4 frag_surf(v2f_surf IN) : SV_Target
				{
					half alpha = tex2D(_MainTex, IN.hip_pack0.xy).a;
					clip(alpha - _Cutoff);
					return 0;
				}
				ENDCG
			}
	}
}