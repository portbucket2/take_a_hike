﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexUnitAttributes : MonoBehaviour
{
    public GameObject[] cornerSpheres;
    public float sizeScale;
    public int targetStation;
    public int tempTargetStation;

    public GameObject affectingBone0;
    public GameObject affectingBone1;
    public GameObject subBone0;
    public GameObject subBone1;
    public float boneWeight;
    //public bool BoneMode;
    public BranchCreationSystem BranchCreationSystem;
    public bool activated;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < cornerSpheres.Length; i++)
        {
            cornerSpheres[i].transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        

        if (BranchCreationSystem. BoneMode)
        {
            FollowBones();
        }
        else
        {
            ResizeCircumferance();
        }
        //FollowBones();
    }

    public void ResizeCircumferance()
    {
        if (activated)
        {
            for (int i = 0; i < cornerSpheres.Length; i++)
            {
                cornerSpheres[i].transform.localPosition = new Vector3(0, 0, sizeScale);
            }
        }
        else
        {
            for (int i = 0; i < cornerSpheres.Length; i++)
            {
                //cornerSpheres[i].transform.localPosition = new Vector3(0, 0, 0);
            }
        }
        
    }

    public void FollowBones()
    {
        if (subBone1)
        {
            transform.position = Vector3.Lerp(subBone0.transform.position, subBone1.transform.position, boneWeight);
            transform.rotation = Quaternion.Lerp(subBone0.transform.rotation, subBone1.transform.rotation, boneWeight);
        }
        else
        {
            transform.position =subBone0.transform.position;
            transform.rotation = subBone0.transform.rotation;
        }
        
    }
}
