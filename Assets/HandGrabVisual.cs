﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandGrabVisual : MonoBehaviour
{
    public Camera cam;
    public Sprite HandOpen;
    public Sprite HandGrabbed;
    public Image HandImage;
    // Start is called before the first frame update
    void Start()
    {
        HandImage = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = cam.ScreenToViewportPoint( new Vector3(Input.mousePosition.x * Screen.width , Input.mousePosition.y * Screen.height , 0));

        if (Input.GetMouseButton(0))
        {
            HandImage.sprite = HandGrabbed;
        }
        else
        {
            HandImage.sprite = HandOpen;
        }
    }
}
