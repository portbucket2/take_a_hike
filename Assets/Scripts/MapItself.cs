﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapItself : MonoBehaviour
{
    public static MapItself instance;
    public Transform mapHolder;
    public Transform mapItemsHolder;
    public int mapPointAppIndex;
    public int mapPointAppIndexMax;
    public List<GameObject> mapItemsCollected;
    // Start is called before the first frame update
    public void Awake()
    {
        instance = this;
        Debug.Log("Mapp");
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdatemapPointAppIndex()
    {
        if(mapPointAppIndex <mapPointAppIndexMax)
        {
            mapPointAppIndex += 1;
        }
        else
        {
            mapPointAppIndex = 0;
        }
    }

    public void AddItemToMap(GameObject it)
    {
        if(mapItemsCollected.Count > 1)
        {
            for (int i = 0; i < mapItemsCollected.Count; i++)
            {
                if(it.transform.localPosition.z == mapItemsCollected[i].transform.localPosition.z)
                {
                    it.transform.localPosition += new Vector3(0, 0, -.05f);
                }
            }


        }
        mapItemsCollected.Add(it);
    }
}
