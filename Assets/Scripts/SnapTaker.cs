﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnapTaker : MonoBehaviour
{
    public static SnapTaker instance;
    public Camera cam;
    Texture2D snapShot;
    public Image snapImage;
    //public SpriteRenderer snapImageSprite;
    Sprite snapSprite;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        cam = Camera.main;
        //TakeSnap();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeSnap();
        }
    }

    public void TakeSnap()
    {
        RenderTexture currentActiveRT = new RenderTexture(900, 1600, 24);
        cam.targetTexture = currentActiveRT;

        
        cam.Render();
        RenderTexture.active = currentActiveRT;
        Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height,TextureFormat.RGB24, false);
        image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        image.Apply();
        snapImage.sprite = Sprite.Create(image, new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), new Vector2(0.5f, 0.5f));
        //snapImageSprite.sprite = Sprite.Create(image, new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), new Vector2(0.5f, 0.5f));

        cam.targetTexture = null;
        RenderTexture.active = null;
    }
}
