﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubPathPoints : MonoBehaviour
{
    public Transform targetPointFront;
    public Transform targetPointBack;

    public GameObject mapPoint;
    public GameObject myMapPoint;
    bool mapPointUpdated;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreatMapPoint()
    {
        myMapPoint = Instantiate(mapPoint, transform.position, transform.rotation);
        myMapPoint.transform.SetParent(MapItself.instance.mapHolder);
        myMapPoint.SetActive(false);

    }
    public void VisibleMapPoint()
    {
        //myMapPoint.SetActive(false);
        if (!mapPointUpdated)
        {
            MapItself.instance.UpdatemapPointAppIndex();
            if(MapItself.instance.mapPointAppIndex == 0)
            {
                myMapPoint.SetActive(true);
            }
            
            mapPointUpdated = true;
        }
        
    }


}
