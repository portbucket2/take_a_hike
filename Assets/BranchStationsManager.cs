﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchStationsManager : MonoBehaviour
{
    public BranchStation[] BranchStationsSource;
    public int BranchStationsSize;
    public BranchStation[] BranchStations;
    public int subSteps;
    public List<GameObject> subStationsList;

    public GameObject subStation;
    float increament;

    public BranchCreationSystem branchCreationSystem;
    public GameObject branchStationsHolder;
    public GameObject bonesHolder;
    public GameObject bone;
    public GameObject[] bones;
    public BranchWaveAnimCreation BranchWaveAnimCreation;
    
    // Start is called before the first frame update
    void Awake()
    {
        if(BranchStationsSize > BranchStationsSource.Length)
        {
            BranchStationsSize = BranchStationsSource.Length;
        }
        BranchStations = new BranchStation[BranchStationsSize];
        for (int i = 0; i < BranchStations.Length; i++)
        {
            BranchStations[i] = BranchStationsSource[i];
        }
        increament = 1f / (float)subSteps;
        bones = new GameObject[BranchStations.Length];
        BranchWaveAnimCreation.bones = new GameObject[BranchStations.Length -1];
        BranchWaveAnimCreation.sineValues = new float[BranchStations.Length -1];
        for (int j = 0; j < BranchStations.Length - 1; j++)
        {
            for (float i = 0.01f; i < 1f; i += increament)
            {
                //for (int j = 0; j < 2; j++)
                //{
                //    //CreatSubPoints(j, j+1, i);
                //}
                CreatSubPoints(j, j + 1, i);

                

            }
            CreatBone(BranchStations[j].gameObject.transform,j);
            if (j > 0)
            {
                bones[j].transform.SetParent(bones[j - 1].transform);
            }
        }
        
        for (float i = 0.01f; i < 1f; i += increament)
        {
            //CreatSubPoints(1,2, i);
        }

        branchCreationSystem.BranchStations = new Transform[subStationsList.Count];
        for (int i = 0; i < subStationsList.Count; i++)
        {
            if(i > 0)
            {
                Quaternion dir = Quaternion.FromToRotation(Vector3.up, -(subStationsList[i - 1].transform.position - subStationsList[i].transform.position));
                subStationsList[i].transform.rotation = dir;
            }
            else
            {
                //Quaternion dir = Quaternion.LookRotation(Vector3.up , -(subStationsList[i - 1].transform.position - subStationsList[i].transform.position));
                subStationsList[i].transform.rotation = transform.rotation;
            }
            branchCreationSystem.BranchStations[i] = subStationsList[i].transform;
            //subStationsList[i].transform.LookAt(subStationsList[i + 1].transform.position, Vector3.forward);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreatSubPoints(int from , int to , float fraction)
    {
        Vector3 pos1 = Vector3.Lerp(BranchStations[from].transform.position, BranchStations[from].Uppoint.position, fraction);
        Vector3 pos2 = Vector3.Lerp(BranchStations[from].Uppoint.position, BranchStations[to].DownPoint.position, fraction);
        Vector3 pos3 = Vector3.Lerp(BranchStations[to].DownPoint.position, BranchStations[to].transform.position, fraction);

        Vector3 pos4 = Vector3.Lerp(pos1, pos2, fraction);
        Vector3 pos5 = Vector3.Lerp(pos2, pos3, fraction);

        Vector3 pos6 = Vector3.Lerp(pos4, pos5, fraction);

        //Quaternion dir = 

        GameObject Go =  Instantiate(subStation, pos6, Quaternion.identity);
        Go.transform.SetParent(branchStationsHolder.transform);
        subStationsList.Add(Go);
    }

    void CreatBone(Transform t , int i)
    {
        //GameObject go = Instantiate(bone, t.position, t.rotation);
        GameObject go = Instantiate(bone, t.position, transform.rotation);
        bones[i] = go;
        bones[i].name = "bone" + i;
        bones[i].transform.SetParent(bonesHolder.transform);
        BranchWaveAnimCreation.bones[i] = bones[i];
    }
}
