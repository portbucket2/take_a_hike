﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManaging : MonoBehaviour
{
    public int coinsCollected;
    public float coinsShown;
    public float coinsShownLevelComplete;
    public Text coinsText;
    public Text levelCompleteCoinsText;

    public GameObject levelCompleteBoard;


    public static UiManaging instance;

    public bool Levelcomplete;
    public ParticleSystem backGlowParticle;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCoins();
        if (Levelcomplete)
        {
            LevelCompleteBoard();
        }
    }


    void UpdateCoins()
    {
        coinsText.text = ""+(int)coinsShown;
        

        if (coinsShown < coinsCollected)
        {
            coinsShown += Time.deltaTime*10;
            

        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            
            //EnableTree();
        }
        if(coinsShown >= 130 && !Levelcomplete)
        {
            TriggerlevelComplete();
        }

    }

    void LevelCompleteBoard()
    {
        levelCompleteCoinsText.text = "" + (int)coinsShownLevelComplete;


        if (coinsShownLevelComplete < coinsCollected)
        {
            coinsShownLevelComplete += Time.deltaTime * 50;
        }
        else
        {
            coinsShownLevelComplete = coinsCollected;
        }
    }

    void TriggerlevelComplete()
    {
        levelCompleteBoard.SetActive(true);
        Levelcomplete = true;
        backGlowParticle.gameObject.SetActive(true);
        backGlowParticle.Play();
    }
}
