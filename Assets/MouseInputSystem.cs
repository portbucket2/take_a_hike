﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInputSystem : MonoBehaviour
{
    public bool touched;
    public static MouseInputSystem instance;
    public Vector3 mouseTouchPosition;
    //public Text touchText;

    public bool clicked;
    public bool DownClicked;
    public bool OnClickArea;

    public bool SwipedRight;
    public bool SwipedLeft;

    public bool mouseDown;

    public Vector3 clickInPos;
    public Vector3 clickPos;
    public Vector3 clickOutPos;

    public Vector3 ClickFollowerPos;
    public Vector3 SwipeVelocity;
    public Vector3 SwipeProgression;

    public bool rightClick;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mouseTouchPosition = Input.mousePosition;
        if (Input.GetMouseButton(0))
        {
            if (!mouseDown)
            {
                ClickFollowerPos = mouseTouchPosition;
                clickPos = mouseTouchPosition;
                clickInPos = mouseTouchPosition;
                //SwipeVelocity = Vector3.zero;
                SwipeProgression = Vector2.zero;
                mouseDown = true;

                if(mouseTouchPosition.x > Screen.width / 2)
                {
                    rightClick = true;
                }
                else
                {
                    rightClick = false;
                }
            }
            else
            {
                clickPos = mouseTouchPosition;
                SwipeProgression = (clickPos - clickInPos) / Screen.width;

            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            clickOutPos = mouseTouchPosition;
            //SwipeVelocity = (ClickFollowerPos - clickPos) / Screen.width;
            mouseDown = false;
        }

        ClickFollowerPos = Vector3.Lerp(ClickFollowerPos, clickPos, Time.deltaTime * 10);





        if (OnClickArea && DownClicked)
        {
            clicked = true;
            
        }
        else
        {
            clicked = false;

        }
    }
}
