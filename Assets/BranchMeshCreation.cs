﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchMeshCreation : MonoBehaviour
{
    public Mesh BranchMesh;
    MeshFilter BranchMeshFilter;
    public HexUnitAttributes[] HexUnits;
    public List<Vector3> BranchVerticesList;
    public Vector3[] BranchVertices;
    public List<GameObject> BranchVerticesSpheres;
    public int[] BranchTriangles;
    public List<int> BranchTrianglesList;

    public Material treeMat;

    public Vector2[] BranchUv;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBranchMesh();
    }

    public void InitiateBranchMesh()
    {
        for (int i = 0; i < HexUnits.Length; i++)
        {
            for (int j = 0; j < HexUnits[i].cornerSpheres. Length; j++)
            {
                BranchVerticesList.Add(HexUnits[i].cornerSpheres[j].transform.position);
                BranchVerticesSpheres.Add(HexUnits[i].cornerSpheres[j]);
            }
            
        }

        BranchVertices = new Vector3[BranchVerticesList.Count];
        //BranchTriangles = new int[BranchVerticesList.Count];
        for (int i = 0; i < BranchVerticesList.Count; i++)
        {
            BranchVertices[i] = transform.InverseTransformPoint( BranchVerticesList[i]);
            //BranchTriangles[i] = i;
        }

        //GetTriangles(0, 6);
        //GetTriangles(6, 6);
        GetAllTriangles(0, HexUnits.Length - 1);
        BranchMesh = new Mesh();
        this.GetComponent<MeshFilter>().mesh = BranchMesh;
        this.GetComponent<MeshRenderer>().material = treeMat;
        BranchMesh.vertices = BranchVertices;
        BranchMesh.triangles = BranchTriangles;

        BranchMesh.RecalculateNormals();

        CreatUvForBranchBark();
        BranchMesh.uv = BranchUv;
    }

    public void UpdateBranchMesh()
    {
        //for (int i = 0; i < HexUnits.Length; i++)
        //{
        //    for (int j = 0; j < HexUnits[i].cornerSpheres.Length; j++)
        //    {
        //        BranchVertices[(i*j) + j] = (HexUnits[i].cornerSpheres[j].transform.position);
        //    }
        //
        //}

        for (int i = 0; i < BranchVerticesList.Count; i++)
        {


             BranchVertices[i] = transform.InverseTransformPoint(BranchVerticesSpheres[i].transform.position);



        }
        BranchMesh.vertices = BranchVertices;
        BranchMesh.triangles = BranchTriangles;

        BranchMesh.RecalculateNormals();
    }

    public void GetTriangles(int root  , int cycleLength)
    {
        int upRoot = root + cycleLength;
        for (int i = 0; i < cycleLength ; i++)
        {
            if(i < cycleLength - 1)
            {
                BranchTrianglesList.Add(i + root);
                BranchTrianglesList.Add(i + root+1);
                BranchTrianglesList.Add(i + upRoot);

                BranchTrianglesList.Add(i + root + 1);
                BranchTrianglesList.Add(i + upRoot + 1);
                BranchTrianglesList.Add(i + upRoot);
            }
            else
            {
                BranchTrianglesList.Add(i + root);
                BranchTrianglesList.Add(i + root + 1 - cycleLength);
                BranchTrianglesList.Add(i + upRoot);

                BranchTrianglesList.Add(i + root + 1 - cycleLength);
                BranchTrianglesList.Add(i + upRoot + 1 - cycleLength);
                BranchTrianglesList.Add(i + upRoot);
            }
            
        }
        //BranchTriangles = new int[BranchTrianglesList.Count];
        //for (int i = 0; i < BranchTrianglesList.Count; i++)
        //{
        //    
        //    BranchTriangles[i] = BranchTrianglesList[i];
        //}
    }

    public void GetAllTriangles(int startIndex, int MaxCycles)
    {
        for (int i = 0; i < MaxCycles; i++)
        {
            GetTriangles(startIndex , 6);
            startIndex += 6;
        }

        BranchTriangles = new int[BranchTrianglesList.Count];
        for (int i = 0; i < BranchTrianglesList.Count; i++)
        {

            BranchTriangles[i] = BranchTrianglesList[i];
        }
    }

    public void CreatUvForBranchBark()
    {
        BranchUv = new Vector2[BranchVerticesList.Count];

        int verticesPerHex = 6;

        for (int i = 0; i < BranchUv.Length; i++)
        {
            BranchUv[i].x = (((float)(i ) % (float)verticesPerHex))/ (float)(verticesPerHex-1); 
            int yStep = (int)((float)(i) / (float)verticesPerHex);
            int divider =Mathf.CeilToInt( ((float)(BranchUv.Length- verticesPerHex) / (float)verticesPerHex));
            BranchUv[i].y = (float)yStep / divider;
        }
    }

}
