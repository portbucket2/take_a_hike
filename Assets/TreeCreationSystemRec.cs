﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeCreationSystemRec : MonoBehaviour
{
    public Transform[] BranchStations;
    public GameObject HexUnit;
    public HexUnitAttributes[] HexUnitsAttr;
    public GameObject HexUnitsHolder;

    public int[] tempTar;

    public int LastrunningHexIndexes;
    public BranchMeshCreation branchMeshCreation;
    // Start is called before the first frame update
    void Start()
    {
        InitiateBranch();
        tempTar = new int[BranchStations.Length];
        LastrunningHexIndexes = BranchStations.Length - 1;

        branchMeshCreation.InitiateBranchMesh();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = BranchStations.Length - 1; i >0 ; i--)
        {
            MoveTheHex(i);
        }
        
    }

    public void InitiateBranch()
    {
        branchMeshCreation.HexUnits =  HexUnitsAttr = new HexUnitAttributes[BranchStations.Length];
        for (int i = 0; i < BranchStations.Length; i++)
        {
            GameObject Go = Instantiate(HexUnit, BranchStations[0].position, BranchStations[0].rotation);
            Go.transform.SetParent(HexUnitsHolder.transform);
            branchMeshCreation.HexUnits[i] = HexUnitsAttr[i] = Go.GetComponent<HexUnitAttributes>();
            HexUnitsAttr[i].targetStation = i;
            HexUnitsAttr[i].sizeScale = HexUnitsAttr[i].sizeScale *(1 -  ((float)i / (BranchStations.Length-1)));

        }
    }

    public void MoveTheHex(int i )
    {
        if(i >= LastrunningHexIndexes)
        {
            HexUnitsAttr[i].gameObject.transform.position = Vector3.MoveTowards(HexUnitsAttr[i].gameObject.transform.position, BranchStations[tempTar[i]].position, Time.deltaTime * 2);
            HexUnitsAttr[i].gameObject.transform.rotation = Quaternion.RotateTowards(HexUnitsAttr[i].gameObject.transform.rotation, BranchStations[tempTar[i]].rotation, Time.deltaTime * 2* Mathf.Rad2Deg);
            if (HexUnitsAttr[i].gameObject.transform.position == BranchStations[tempTar[i]].position)
            {
                if (tempTar[i] < HexUnitsAttr[i].targetStation)
                {
                    
                    if (tempTar[i] == 1)
                    {
                        LastrunningHexIndexes -= 1;
                        
                    }
                    tempTar[i] += 1;
                }

            }
        }
        
    }
}
