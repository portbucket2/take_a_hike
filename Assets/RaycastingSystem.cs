﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastingSystem : MonoBehaviour
{
    public Camera cam;
    Vector3 mousePos;
    public float mouseOffset;
    RaycastHit hit;
    public bool RayCasted;
    public int triadIndex;

    public bool GotFruit;
    public static RaycastingSystem instance;

    public LeafAttributes currentLeafAttr;
    GameObject go;
    public Vector3 hitPoint;

    public Transform clickedLeafPos;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        go = new GameObject();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Raycasting();
    }

    void Raycasting()
    {
        //mousePos = Input.mousePosition + new Vector3(0, mouseOffset * Screen.width * 0.01f, 0);

        mousePos = Input.mousePosition + new Vector3(0, mouseOffset * Screen.width * 0.01f, 0);
        Ray ray = cam.ScreenPointToRay(mousePos); // Construct a ray from the current mouse coordinates


        if (Physics.Raycast(ray, out hit, 200f))
        {
            Debug.DrawLine(hit.point,-transform.forward, Color.red);
            RayCasted = true;
            hitPoint = hit.point;
            if (Input.GetMouseButtonUp(0))
            {
                GotFruit = false;
            }
            if (Input.GetMouseButton(0))
            {
                
                
                if (hit.transform.gameObject.layer == 9 && !GotFruit)
                {
                    
                    if (!currentLeafAttr)
                    {
                        currentLeafAttr = hit.transform.gameObject.GetComponentInParent<LeafAttributes>();
                        currentLeafAttr.leafParent.GetComponent<BranchWaveAnimCreation>().MyLeafClicked = true;
                        GotFruit = true;
                        //clickedLeafPos = currentLeafAttr.gameObject.transform;
                    }


                    //currentLeafAttr.facingTarget = go.transform;
                    currentLeafAttr.GetFacingTarget(go.transform);

                }
                
            }
            
            else
            {
                GotFruit = false;
                currentLeafAttr = null;
                //clickedLeafPos = null;
            }
            go.transform.position = hit.point;


            //Debug.Log("triss "+ hit.triangleIndex);
            //triadIndex = hit.triangleIndex;

            //Instantiate (particle, hit.point, transform.rotation); // Create a particle if hit
        }
        else
        {
            RayCasted = false;
            GotFruit = false;
            currentLeafAttr = null;
            //clickedLeafPos = null;
        }
        
    }


    public void LostLeafRef()
    {
        currentLeafAttr = null;
    }
}
